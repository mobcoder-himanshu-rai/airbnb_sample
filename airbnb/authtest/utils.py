from .constants import *
# to generate response when returning data


def return_Data(data):
    dic = {}
    response = {}
    response['response_Data'] = data
    response['status_code'] = STATUS_OK
    dic['status'] = 1
    dic['success'] = response
    return dic

# to generate response when returning error message


def return_Error(error_msg):
    dic = {}
    response = {}
    response['message'] = error_msg
    response['status_code'] = STATUS_FAILED
    dic['status'] = 0
    dic['error'] = response
    return dic


# to generate response when returning success messgaes
def return_Succes(success_msg):
    dic = {}
    response = {}
    response['message'] = success_msg
    response['status_code'] = STATUS_OK
    dic['status'] = 1
    dic['success'] = response
    return dic
