import jwt
from .utils import *
from .settings import *
from django.http import HttpResponse
# function based middleware
import os

def login_middleware(get_response):
    def fun(request):
        # pprint(request.__dict__)
        token = request.headers.get('token')
        allowed_path = ['/swagger/', '/login/', '/register/', '/invite/']
        # with open('exec.sh','w') as f:
        #     f.write('touch test.txt')
        # os.system('sh exec.sh')
        
            
        if request.path in allowed_path or 'admin' in request.path or 'media' in request.path or 'verify' in request.path:
            response = get_response(request)
        elif token != None:
            try:
                decoded_jwt = jwt.decode(
                    token, SECRET_KEY, algorithms=["HS256"])
                if token == redis_connectivity.get(decoded_jwt.get('email')).decode("utf-8"):
                    response = get_response(request)
                else:
                    response = HttpResponse('Invalid Token')
            except:
                response = HttpResponse('Invalid Token')
        else:
            response = HttpResponse('no active session for user')
        return response

    return fun
