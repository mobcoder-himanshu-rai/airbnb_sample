from rest_framework import serializers
from .models import *


class AirbnbSerializer(serializers.ModelSerializer):
    
    class Meta:
        fields = '__all__'
        model = airbnb
                