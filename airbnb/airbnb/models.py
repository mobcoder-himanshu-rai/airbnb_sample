# from django.db import models
from djongo import models

class ammenity(models.Model):
    name = models.CharField(max_length=200)
    
    class Meta:
        abstract = True

class airbnb(models.Model):
    _id=models.ObjectIdField()

    listing_url=models.URLField() 
    name    = models.CharField(max_length=200, default="")
    summary     = models.TextField()
    space     = models.TextField()
    description = models.TextField()
    neighborhood_overview = models.TextField()
    notes = models.TextField()
    transit = models.TextField()
    access = models.TextField()
    interaction = models.TextField()
    house_rules = models.TextField()
    property_type = models.TextField()
    room_type = models.TextField()
    minimum_nights = models.TextField()
    maximum_nights = models.TextField()
    cancellation_policy = models.TextField()
    last_scrapped      = models.DateTimeField(auto_now_add=True)
    calendar_last_scrapped = models.DateTimeField(auto_now = True) 
    accommodates = models.IntegerField()
    bedrooms = models.IntegerField()
    beds = models.IntegerField()
    number_of_reviews = models.IntegerField() 
    ammenities = models.JSONField()
    # amenities = models.ArrayField(model_container=ammenity,blank=True)
    price = models.DecimalField(max_digits=10,decimal_places=3)
    extra_people = models.DecimalField(max_digits=10,decimal_places=3)
    guests_included = models.DecimalField(max_digits=10,decimal_places=3)
    address = models.JSONField()
    review_scores = models.JSONField()

    class Meta:
        managed = False
        db_table = "listingsAndReviews"