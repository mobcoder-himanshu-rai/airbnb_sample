# basic URL Configurations
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
# import routers
from rest_framework import routers
# from django.conf.urls import url
from django.contrib import admin
# import everything from views
from .views import *
# define the router
router = routers.DefaultRouter()
# specify URL Path for rest_framework
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('airbnb_country',CountryCount.as_view())

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
