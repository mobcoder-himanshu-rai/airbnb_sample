import csv
from .models import *
from .serializer import *
from authtest.utils import *
from drf_yasg import openapi
from authtest.constants import *
from django.db.models import Count
from rest_framework.views import APIView
from django.db.utils import IntegrityError
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from rest_framework.parsers import MultiPartParser
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.permissions import IsAuthenticated
from authtest.settings import redis_connectivity, SECRET_KEY
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

    
class CountryCount(APIView):
    '''API for getting count and save csv '''
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(tags=["Airbnb"])
    def get(self, request):
        try:
            
            instance = airbnb.objects.values_list('address',flat=True)
            data = {}
            
            for obj in instance.iterator():
                if obj.get('country') in data:
                    data[obj.get('country')] += 1
                else:
                    data[obj.get('country')] = 1
                    
            with open('output.csv', 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=["country","count"])
                writer.writeheader()
                
                for row in data.keys():
                    writer.writerow({'country':row,'count':data[row]})  #we can also upload this file on s3 and return url 

            return Response(return_Data(data))
        
        except Exception as e:
            return Response(return_Error(str(e)))
